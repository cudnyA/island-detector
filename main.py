from islandDetector import IslandDetector
import sys

def readFile(path:str):
    output = []
    with open(path.strip(), 'r') as f:
        for line in f:
            lineOutput = []
            for number in line:
                if ord(number) in [48, 49]:
                    lineOutput.append(int(float(number)))
            output.append(lineOutput)
    return output


if __name__ == '__main__':
    #!/usr/bin/ python
    if len(sys.argv) < 2:
        sys.stderr.write("Please provide a path to a file with zeros and ones as an argument.")
        sys.exit(1)
    out = readFile(sys.argv[1])
    if not out:
        sys.stderr.write("File not recognised.")
        sys.exit(2)
    isleDetector = IslandDetector(out)
    sys.stdout.write(str(isleDetector.countIslands()))
    sys.exit(0)


from itertools import product

class IslandDetector(object):
    def __init__(self, map: list):
        self.rowsCount, self.columnsCount = len(map), len(map[0])
        self.islandCount = 0
        self.map = map
        self.pointsSaved = []

    def countIslands(self):
        for row in range(self.rowsCount):
            for column in range(self.columnsCount):
                if self.map[row][column]:
                    if (row, column) not in self.pointsSaved:
                        self.islandCount += 1
                        self.generatePointList(row, column)
                        # for point in neighbourPoints:
                        #     self.archipelago.addNewPoint(point)
        return (self.islandCount)

    def generatePointList(self, row, column):
        output = [(row, column)]
        x = 0
        while x < len(output):
            output = output + self.checkPointNeighbourhood(output[x][0], output[x][1])
            x = x + 1
        return output

    def checkPointNeighbourhood(self, row, column):
        neighbours = []
        coordinateChangeList = product([-1, 0, 1], repeat=2)
        for coordinatesChange in coordinateChangeList:
            tempRow = row + coordinatesChange[0]
            tempColumn = column + coordinatesChange[1]
            if self.checkCoordinates(tempRow, tempColumn) and self.map[tempRow][tempColumn] and (
            tempRow, tempColumn) not in self.pointsSaved:
                neighbours.append((tempRow, tempColumn))
                self.pointsSaved.append((tempRow, tempColumn))
        return neighbours

    def checkCoordinates(self, row, column):
        if 0 <= row < self.rowsCount and 0 <= column < self.columnsCount:
            return True
        else:
            return False


if __name__ == '__main__':
    sampleInput = [[1, 0, 0, 0, 1, 0, 0, 0, 1],
                   [0, 1, 1, 1, 0, 0, 1, 0, 0],
                   [0, 0, 0, 0, 0, 0, 0, 1, 0],
                   [0, 0, 0, 0, 0, 0, 0, 0, 1],
                   [1, 1, 0, 0, 0, 0, 0, 0, 0],
                   [0, 1, 0, 0, 1, 1, 0, 0, 0],
                   [0, 0, 0, 0, 1, 0, 0, 0, 0],
                   [1, 0, 0, 0, 1, 1, 0, 0, 1]]
    print(sampleInput[1][1])
    isleDetector = IslandDetector(sampleInput)
    print(isleDetector.countIslands())

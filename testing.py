import os
import time
import islandDetector
import main

mainPath = os.getcwd()+"/sampleFiles"
for file in os.listdir(mainPath):
    if "wrong" not in file:
        start = time.time()
        map = main.readFile(mainPath + "/" + file)
        isleDetector = islandDetector.IslandDetector(map)
        assert str(isleDetector.countIslands()) == file[:-4]
        end = time.time()
        print(file + "; Time: " + str(end - start) + "s")
